

//A "package" in Java is used to group related classes. Think of it as a folder in a file directory.
// Packages are divided into two categories:
    // 1.Built-in Packages (packages from the Java API)
    // 2. User-defined Packages (create your own packages)

// Package creation follows the "reverse domain name notation" for the naming convention.
    // The syntax is useful because the ordering of the named components gets reversed, surfacing the logical groupings inherent in the designed structure of DNS.

package com.zuitt.example;
public class Variables {
    public static void main(String[] args){
        // Naming convention
            // The terminology used for variable names is "identifier".
            // All identifiers should begin with a letter (A to Z or a to z), currency character ($) or an underscore. not allowed: 1x=9
            // After the first character, identifiers can have any combination of characters.
            // A keyword cannot be used as an identifier.
            // Most importantly, identifiers are case-sensitive. CaSe Sensitive siya
            // Syntax: dataType identifier (camel case)

            //variable

            //age; (ano tong age na to)
            int age;
            char middleName;

            //variable declaration vs initialization
            int x;
            int y = 0;

            //initialization after declaration
            x = 1;

            //output to the systems
        System.out.println("The value of y is " + y + " and the value of x is " + x);
            //we need a delimiter ;, end of statement

        //Primitive Data Types
            //predefined within the Java programming language which is used for single-valued variables with limited capabilities.

        // int - whole number values
        int wholeNumber = 100;
        System.out.println(wholeNumber);

        //long
        //L is added to the end of the long number to be recognized

        long worldPopulation = 78628811457878L;
        System.out.println(worldPopulation);

        //ctrl shift f10

        //float
        //add f at the end of the float to be recognized

        float piFloat = 3.14159265359f;
        System.out.println(piFloat);

        //double - floating point values
        //float 7 decimal places

        double piDouble = 3.14159265359;
        System.out.println(piDouble);

        //char - single characters
        //java does not use "" double quotes in characters
        //double quotes - string
        char letter = 'a';
        System.out.println(letter);

        //boolean
        boolean isLove = true;
        boolean isTaken = false;

        System.out.println(isLove);
        System.out.println(isTaken);

        //constants
        //Java uses the final keyword so the variable's value cannot be changed
        //naming convention sa const or sa constants ay UPPERCASE
        final int PRINCIPAL = 3000;
        System.out.println(PRINCIPAL);
        //PRINCIPAL = 4000;

        // Non-primitive data
            // Also known as reference data types refer to instances or objects.
            // do not directly store the value of a variable, but rather remembers the reference to that variable.

        // String
        // stores a sequence or array of characters.
        // String are actually object that can use methods

        //we can access all characters

        String username = "JSmith";
        System.out.println(username);

        //Sample string method
        int stringLength = username.length();
        System.out.println(stringLength);

    }
}
