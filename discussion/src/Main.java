
// Main Class
    // entry point for our java program
    // main class has 1 method inside, the main method => to run our code

//public accessible to all
//static inaalow tayo magrun without instantiating
//JAVA creates classes na may properties and methods
//void wala tayo irereturn na data type, it doesn't return anything
//static may access sa main method
// public - access modifier which simple tells the application which classes have access to method / attributes.
// static - keyword associated with a method/property that is related in a class. This will allow a method to be invoked without instantiating a class.
// void - a keyword that is used to specify a method that doesn't return anything. In java we have to declare the data type of the method's return.
// String[] args - accepts a single argument of type String array that contains command line argument.
// command line arguments is use to affect the operation of the program, or to pass information to the program, at runtime.

public class Main {
    public static void main(String[] args) {
        //to print statement in the terminal
        System.out.println("Hello world yeah!");
        System.out.println("Yeah!");
    }
}
